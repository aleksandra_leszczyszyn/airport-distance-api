﻿namespace AirportsDistanceApi.BusinessLogic
{
    public interface IDistanceService
    {
        double GetDistance(string iata1, string iata2);
    }
}
