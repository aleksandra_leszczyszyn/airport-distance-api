﻿using System;
using AirportsDistanceApi.BusinessLogic;
using AirportsDistanceApi.BusinessLogic.Models;
using AirportsDistanceApi.DataAccess;

namespace AirportsDistanceApi
{
    public class DistanceService : IDistanceService
    {
        private const double earthRadius = 3958.8; // TO DO change km to miles !!!

        private IAirportsRepository airportsRepository;

        public DistanceService(IAirportsRepository airportsRepository)
        {
            this.airportsRepository = airportsRepository;
        }

        public double GetDistance(string iata1, string iata2)
        {
            this.ValidateIataCodes(iata1, iata2);
            Airport airport1 = this.airportsRepository.GetByIataAsync(iata1);
            Airport airport2 = this.airportsRepository.GetByIataAsync(iata2);
            double distance = this.GetDistance(airport1, airport2);
            return distance;
        }

        private void ValidateIataCodes(string iata1, string iata2)
        {
            if (iata1.Length != 3 || iata2.Length != 3)
            {
                throw new FormatException("IATA code must contain 3 characters.");
            }
        }

        private double GetDistance(Airport airport1, Airport airport2)
        {
            double latitude1 = airport1.Location.Lat;
            double longitude1 = airport1.Location.Lon;
            double latitude2 = airport2.Location.Lat;
            double longitude2 = airport2.Location.Lon;

            double latDifference = this.GetRadiusFromDegree(latitude1 - latitude2);
            double lonDifference = this.GetRadiusFromDegree(longitude1 - longitude2);

            double a = Math.Sin(latDifference / 2) * Math.Sin(latDifference / 2) +
                Math.Cos(GetRadiusFromDegree(latitude1)) * Math.Cos(GetRadiusFromDegree(latitude2)) *
                Math.Sin(lonDifference / 2) * Math.Sin(lonDifference / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double d = Math.Round(earthRadius * c, 2);

            return d;
        }

        private double GetRadiusFromDegree(double degree)
        {
            return degree * (Math.PI / 180);
        }
    }
}