﻿namespace AirportsDistanceApi.BusinessLogic.Models
{
    public class Airport
    {
        public string Iata { get; set; }
        public string City { get; set; }
        public Location Location { get; set; }
    }
}