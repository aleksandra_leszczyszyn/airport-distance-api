﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using AirportsDistanceApi.BusinessLogic.Models;
using Newtonsoft.Json;

namespace AirportsDistanceApi.DataAccess
{
    public class AirportsRepository : IAirportsRepository
    {
        private const string URL = "https://places-dev.cteleport.com/airports/";

        public Airport GetByIataAsync(string iata)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(iata).Result;
            Airport airport = this.GetAirportFromResponse(response, iata);
            return airport;
        }

        private Airport GetAirportFromResponse(HttpResponseMessage response, string iata)
        {
            Airport airport = new Airport();
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                airport = JsonConvert.DeserializeObject<Airport>(result);
            }
            else
            {
                throw new Exception("Airport " + iata + " not found.");
            }
            return airport;
        }
    }
}
