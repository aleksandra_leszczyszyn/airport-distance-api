﻿using AirportsDistanceApi.BusinessLogic.Models;

namespace AirportsDistanceApi.DataAccess
{
    public interface IAirportsRepository
    {
        Airport GetByIataAsync(string iata);
    }
}
