﻿using System;
using AirportsDistanceApi.BusinessLogic;
using Microsoft.AspNetCore.Mvc;

namespace AirportsDistanceApi.Controllers
{
    [Route("api/[controller]")]
    public class DistanceController : Controller
    {
        private readonly IDistanceService distanceService;

        public DistanceController(IDistanceService distanceService)
        {
            this.distanceService = distanceService;
        }

        /// <summary>
        /// Gets distance in miles between two airports
        /// </summary>
        /// <param name="iata1"></param>
        /// <param name="iata2"></param>
        /// <returns>Distance in miles</returns>
        [HttpGet("{iata1},{iata2}")]
        public IActionResult GetDistance(string iata1, string iata2)
        {
            try
            {
                double distance = this.distanceService.GetDistance(iata1, iata2);
                return new OkObjectResult(new { distance });
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(ex.Message);
            }
        }
    }
}