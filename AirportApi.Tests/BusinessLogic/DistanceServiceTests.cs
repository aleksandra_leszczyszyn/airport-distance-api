﻿using AirportsDistanceApi;
using AirportsDistanceApi.BusinessLogic.Models;
using AirportsDistanceApi.DataAccess;
using Moq;
using NUnit.Framework;
using System;

namespace AirportsDistanceApi.Tests.BusinessLogic
{
    [TestFixture]
    public class DistanceServiceTests
    {
        private IAirportsRepository mockedAirportsRepository;

        [SetUp]
        public void SetupBeforeTest()
        {
            Airport airport = this.GetMockedAirport();
            var repository = new Mock<IAirportsRepository>();
            repository.Setup(e => e.GetByIataAsync(It.IsAny<string>()))
                .Returns(airport);
            this.mockedAirportsRepository = repository.Object;
        }

        private Airport GetMockedAirport()
        {
            Airport airport = new Airport();
            Location location = new Location() { Lat = 12, Lon = 0 };
            airport.Location = location;
            return airport;
        }

        [TestCase]
        public void TestGetDistance()
        {
            DistanceService distanceService = 
                new DistanceService(this.mockedAirportsRepository);

            double result = distanceService.GetDistance("WRO", "AMS");

            Assert.That(result, Is.TypeOf<double>());
            Assert.That(result, Is.GreaterThanOrEqualTo(0));
        }

        [TestCase]
        public void TestGetDistance_FailsWhenWrongIata()
        {
            DistanceService strategy =
                new DistanceService(this.mockedAirportsRepository);

            TestDelegate result = () => strategy.GetDistance("WRO", "AMSS");
            
            Assert.That(result, Throws.TypeOf<FormatException>()
                .With.Message.EqualTo("IATA code must contain 3 characters."));
        }
    }
}
