using AirportsDistanceApi.BusinessLogic.Models;
using AirportsDistanceApi.DataAccess;
using NUnit.Framework;

namespace AirportsDistanceApi.Tests
{
    [TestFixture]
    public class AirportsRepositoryTests
    {
        [TestCase]
        public void TestGetByIataAsync()
        {
            AirportsRepository repository = new AirportsRepository();
            string iata = "WRO";

            Airport result = repository.GetByIataAsync(iata);

            Assert.That(result, Is.TypeOf<Airport>());
            Assert.That(result.Iata, Is.EqualTo(iata));
            Assert.That(result.Location.Lat, Is.Not.Null);
            Assert.That(result.Location.Lon, Is.Not.Null);
        }

        [TestCase]
        public void TestGetByIataAsync_FailsWhenWrongIata()
        {
            AirportsRepository repository = new AirportsRepository();
            string iata = "WWJW";

            TestDelegate result = () => repository.GetByIataAsync(iata);

            Assert.That(result, Throws.Exception
                .With.Message.EqualTo("Airport " + iata + " not found."));
        }
    }
}